<?php

namespace Modules\Game\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Game\Repositories\Eloquent\GameSessionRepository;
use Modules\Game\Repositories\Interfaces\GameSessionRepositoryInterface;

class RepositoryServiceProvider extends ServiceProvider
{
    public array $bindings = [
        GameSessionRepositoryInterface::class => GameSessionRepository::class,
    ];
}
