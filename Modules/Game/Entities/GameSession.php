<?php

namespace Modules\Game\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Game\DTO\GridDTO;
use Spatie\LaravelData\WithData;

/**
 * @property string $uuid
 * @property GridDTO $player_grid
 * @property GridDTO $computer_grid
 * @property string $ended_at
 */
class GameSession extends Model
{
    use WithData;
    protected $fillable = ['player_grid', 'computer_grid', 'ended_at'];
    protected $casts = [
        'player_grid' => GridDTO::class,
        'computer_grid' => GridDTO::class,
        'ended_at' => 'datetime',
    ];
}
