<?php

namespace Modules\Game\Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Modules\Game\Entities\GameSession;
use Tests\TestCase;

class GameActionsTest extends TestCase
{
    use DatabaseTransactions;

    public function testShoot(): void
    {
        $session = $this->getJson(route('game.session.create'))->json('uuid');

        $response = $this->postJson(route('game.actions.shoot'), [
            'uuid' => $session,
            'x' => 1,
            'y' => 5,
        ]);

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'uuid',
            'player_grid',
            'computer_grid',
            'ended_at',
        ]);

        $response->assertJson([
            'computer_grid' => [
                'hits' => [
                    [
                        'x' => 1,
                        'y' => 5,
                    ],
                ],
            ],
            'player_grid' => [
                'hits' => [
                    array(),
                ],
            ],
        ]);
    }

    public function testShipSunk(): void
    {
        $session = $this->getJson(route('game.session.create'))->json('uuid');
        $computerShips = GameSession::where('uuid', $session)->first()->computer_grid->ships->toArray();
        $ship = $computerShips[0];

        foreach ($ship['coordinates'] as $coordinate) {
            $response = $this->postJson(route('game.actions.shoot'), [
                'uuid' => $session,
                'x' => $coordinate['x'],
                'y' => $coordinate['y'],
            ]);

            $response->assertStatus(200);

            $ships = $response->json('computer_grid')['ships'];

            if (count($ships) === 1 && $ships[0]['isSunk'] === true) {
                $this->assertTrue(true);

                return;
            }
        }

        $this->fail('Ship should have sunk before this point');
    }

    public function testGameEnded(): void
    {
        $session = $this->getJson(route('game.session.create'))->json('uuid');
        $game = GameSession::where('uuid', $session)->first();

        foreach ($game->computer_grid->ships as $ship) {
            foreach ($ship->coordinates as $coordinate) {
                $response = $this->postJson(route('game.actions.shoot'), [
                    'uuid' => $session,
                    'x' => $coordinate->x,
                    'y' => $coordinate->y,
                ]);

                $response->assertStatus(200);

                if($response->json('ended_at') !== null) {
                    $unsunkEnemyShips = array_filter($response->json('computer_grid')['ships'], function ($ship) {
                        return $ship['isSunk'] === false;
                    });

                    $unsunkShips = array_filter($response->json('player_grid')['ships'], function ($ship) {
                        return $ship['isSunk'] === false;
                    });

                    $this->assertTrue(count($unsunkEnemyShips) === 0 || count($unsunkShips) === 0);

                    return;
                }
            }
        }

        $this->fail('Game should have ended before this point');
    }
}
