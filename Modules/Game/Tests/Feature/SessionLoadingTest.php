<?php

namespace Modules\Game\Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Modules\Game\Entities\GameSession;
use Tests\TestCase;

class SessionLoadingTest extends TestCase
{
    use DatabaseTransactions;

    public function testSessionLoad(): void
    {
        $session = $this->getJson(route('game.session.create'))->json('uuid');

        $response = $this->postJson(route('game.session.load', [
            'uuid' => $session,
        ]));

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'uuid',
            'player_grid',
            'computer_grid',
            'ended_at',
        ]);
    }

    public function testSessionLoadNotFound(): void
    {
        $response = $this->postJson(route('game.session.load', [
            'uuid' => 'not-found',
        ]));

        self::assertNull($response->json('uuid'));
    }

    public function testIfSessionDontShowEnemyShips(): void
    {
        $session = $this->getJson(route('game.session.create'))->json('uuid');

        $response = $this->postJson(route('game.session.load', [
            'uuid' => $session,
        ]));

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'uuid',
            'player_grid',
            'computer_grid',
            'ended_at',
        ]);

        $response->assertJson([
            'computer_grid' => [
                'ships' => [],
            ],
        ]);
    }

    public function testIfSessionShowEnemyShipsIfSunk(): void
    {
        $session = $this->getJson(route('game.session.create'))->json('uuid');

        $updateSunk = GameSession::where('uuid', $session)->first();
        $updateSunk->computer_grid->ships[0]->isSunk = true;
        $updateSunk->save();

        $response = $this->postJson(route('game.session.load', [
            'uuid' => $session,
        ]));

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'uuid',
            'player_grid',
            'computer_grid',
            'ended_at',
        ]);

        $response->assertJson([
            'computer_grid' => [
                'ships' => [
                    [
                        'isSunk' => true,
                    ],
                ],
            ],
        ]);
    }
}
