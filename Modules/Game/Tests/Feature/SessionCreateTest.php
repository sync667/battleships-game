<?php

namespace Modules\Game\Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class SessionCreateTest extends TestCase
{
    use DatabaseTransactions;

    public function testCreateSession(): void
    {
        $response = $this->getJson(route('game.session.create'));

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'uuid',
        ]);
        $this->assertDatabaseHas('game_sessions', [
            'uuid' => $response->json('uuid'),
        ]);
    }
}
