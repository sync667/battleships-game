<?php

namespace Modules\Game\Tests\Unit;

use Modules\Game\DTO\CoordsDTO;
use Modules\Game\Http\Services\ShipsGenerationService;
use Tests\TestCase;

class ShipsGenerationServiceTest extends TestCase
{
    public function testGenerateShipCoordinates()
    {
        $service = new ShipsGenerationService();
        $gridSize = 10;
        $size = 4;
        $shipCoordinates = [];

        $coordinates = $service->generateShipCoordinates($shipCoordinates, $gridSize, $size);

        $this->assertCount($size, $coordinates);

        foreach ($coordinates as $coordinate) {
            $this->assertInstanceOf(CoordsDTO::class, $coordinate);
        }

        foreach ($coordinates as $coordinate) {
            $this->assertGreaterThanOrEqual(0, $coordinate->x);
            $this->assertLessThan($gridSize, $coordinate->x);
            $this->assertGreaterThanOrEqual(0, $coordinate->y);
            $this->assertLessThan($gridSize, $coordinate->y);
        }
    }
}
