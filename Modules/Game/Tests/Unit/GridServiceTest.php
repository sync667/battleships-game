<?php

namespace Modules\Game\Tests\Unit;

use Modules\Game\DTO\GridDTO;
use Modules\Game\Http\Services\GridService;
use Modules\Game\Http\Services\ShipsGenerationService;
use Tests\TestCase;

class GridServiceTest extends TestCase
{
    public function testGenerateShipsGrid()
    {
        $shipsGenerationService = new ShipsGenerationService();
        $gridService = new GridService($shipsGenerationService);

        $gridDTO = $gridService->generateShipsGrid();

        $this->assertInstanceOf(GridDTO::class, $gridDTO);
        $this->assertEquals(count(config('game.default_ships')), $gridDTO->ships->count());
        $this->assertEquals(config('game.default_ships')[0]->size, $gridDTO->ships[0]->size);
        $this->assertEquals(config('game.default_ships')[0]->size, $gridDTO->ships[0]->coordinates->count());
    }
}
