<?php

namespace Modules\Game\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GameLoadRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'uuid' => 'required|uuid|exists:game_sessions,uuid',
        ];
    }
}
