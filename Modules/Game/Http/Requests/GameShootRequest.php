<?php

namespace Modules\Game\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GameShootRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'uuid' => 'required|uuid|exists:game_sessions,uuid',
            'x' => 'required|integer|min:0|max:9',
            'y' => 'required|integer|min:0|max:9',
        ];
    }
}
