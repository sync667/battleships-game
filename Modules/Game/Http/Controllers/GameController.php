<?php

namespace Modules\Game\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller;
use Modules\Game\Http\Requests\GameLoadRequest;
use Modules\Game\Http\Services\GameLoadService;
use Modules\Game\Http\Services\GameSessionService;

class GameController extends Controller
{
    public function __construct(
        private GameLoadService    $gameLoadService,
        private GameSessionService $gameSessionService
    ) {
    }

    public function createSession(): JsonResponse
    {
        return response()->json(['uuid' => $this->gameSessionService->create()]);
    }

    public function loadGame(GameLoadRequest $request): JsonResponse
    {
        return response()->json($this->gameLoadService->loadGame($request->validated('uuid')));
    }
}
