<?php

namespace Modules\Game\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller;
use Modules\Game\Http\Requests\GameShootRequest;
use Modules\Game\Http\Services\GameShootService;

class GameActionsController extends Controller
{
    public function __construct(
        private GameShootService $gameShootService,
    ) {
    }

    public function shoot(GameShootRequest $request): JsonResponse
    {
        return response()->json($this->gameShootService->shoot(
            $request->validated('uuid'),
            $request->validated('x'),
            $request->validated('y')
        ));
    }
}
