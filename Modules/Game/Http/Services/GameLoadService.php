<?php

namespace Modules\Game\Http\Services;

use Modules\Game\DTO\GameSessionDTO;
use Modules\Game\Repositories\Interfaces\GameSessionRepositoryInterface;

class GameLoadService
{
    public function __construct(
        private GameSessionRepositoryInterface $gameSessionRepository,
    ) {
    }

    public function loadGame(string $uuid): ?GameSessionDTO
    {
        $gameSession = $this->gameSessionRepository->loadGame($uuid);

        $gameSession->computer_grid->ships = $gameSession->computer_grid->ships->where('isSunk', '=', true)->values();

        return $gameSession;
    }
}
