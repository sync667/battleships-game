<?php

namespace Modules\Game\Http\Services;

use Modules\Game\DTO\CoordsDTO;
use Modules\Game\DTO\GridDTO;
use Modules\Game\DTO\ShipDTO;

class GridService
{
    public function __construct(private ShipsGenerationService $shipsGenerationService)
    {
    }

    public function generateShipsGrid(): GridDTO
    {
        $shipCoordinates = [];

        foreach (config('game.default_ships') as $ship) {
            $coordinates = $this->shipsGenerationService
                ->generateShipCoordinates($shipCoordinates, config('game.grid_size'), $ship->size);

            $shipCoordinates[] = ShipDTO::from([
                'type' => $ship->type,
                'size' => $ship->size,
                'isSunk' => false,
                'coordinates' => CoordsDTO::collection($coordinates),
            ]);
        }

        return GridDTO::from([
            'size' => config('game.grid_size'),
            'ships' => ShipDTO::collection($shipCoordinates),
        ]);
    }
}
