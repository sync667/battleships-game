<?php

namespace Modules\Game\Http\Services;

use Modules\Game\DTO\GameSessionDTO;
use Modules\Game\Repositories\Interfaces\GameSessionRepositoryInterface;

class GameShootService
{
    public function __construct(
        private GameSessionRepositoryInterface $gameSessionRepository,
    ) {
    }

    public function shoot(string $uuid, int $x, int $y): GameSessionDTO
    {
        $gameSession = $this->gameSessionRepository->loadGame($uuid);

        if ($gameSession->isGameEnded()) {
            return $gameSession;
        }

        if($gameSession->computer_grid->shot($x, $y) && $gameSession->player_grid->computerShoot()) {
            $this->gameSessionRepository->saveHits($uuid, $gameSession->player_grid, $gameSession->computer_grid);
        }

        if($gameSession->computer_grid->isAllShipsSunk() || $gameSession->player_grid->isAllShipsSunk()) {
            $gameSession->ended_at = $this->gameSessionRepository->endGame($uuid);

            return $gameSession;
        }

        $gameSession->computer_grid->ships = $gameSession->computer_grid->ships->where('isSunk', '=', true)->values();

        return $gameSession;
    }
}
