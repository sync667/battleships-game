<?php

namespace Modules\Game\Http\Services;

use Modules\Game\DTO\CoordsDTO;

class ShipsGenerationService
{
    public function generateShipCoordinates(array $shipCoordinates, int $gridSize, int $size): array
    {
        $orientation = rand(0, 1); // 0 - horizontal, 1 - vertical

        $startRow = rand(0, $gridSize - ($orientation ? $size : 1));
        $startCol = rand(0, $gridSize - ($orientation ? 1 : $size));

        $coordinates = [];
        for ($i = 0; $i < $size; $i++) {
            $coordinates[] = CoordsDTO::from([
                'x' => $orientation ? $startRow + $i : $startRow,
                'y' => $orientation ? $startCol : $startCol + $i,
            ]);
        }

        if($this->isPlacementValid($shipCoordinates, $coordinates)) {
            return $coordinates;
        }

        return $this->generateShipCoordinates($shipCoordinates, $gridSize, $size);
    }

    private function isPlacementValid(array $ships, array $coordinates): bool
    {
        $neighbours = [
            [-1, -1], [-1,  0], [-1,  1],
            [ 0, -1], [ 0,  0], [ 0,  1],
            [ 1, -1], [ 1,  0], [ 1,  1],
        ];

        foreach ($coordinates as $shipPartCoords) {
            foreach ($ships as $ship) {
                foreach ($ship->coordinates as $coords) {
                    // collision with other ship
                    if ($coords->x === $shipPartCoords->x && $coords->y === $shipPartCoords->y) {
                        return false;
                    }

                    // collision with neighbour cords
                    foreach ($neighbours as $nearbyCoord) {
                        $nearbyX = $coords->x + $nearbyCoord[0];
                        $nearbyY = $coords->y + $nearbyCoord[1];
                        if ($nearbyX === $shipPartCoords->x && $nearbyY === $shipPartCoords->y) {
                            return false;
                        }
                    }
                }
            }
        }

        return true;
    }
}
