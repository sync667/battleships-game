<?php

namespace Modules\Game\Http\Services;

use Modules\Game\Repositories\Interfaces\GameSessionRepositoryInterface;

class GameSessionService
{
    public function __construct(private GameSessionRepositoryInterface $gameSessionRepository)
    {
    }

    public function create(): string
    {
        return $this->gameSessionRepository->createGame();
    }
}
