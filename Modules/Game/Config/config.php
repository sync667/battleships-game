<?php

use Modules\Game\DTO\ShipDTO;

return [
    'name' => 'Game',
    'grid_size' => 10,
    'default_ships' => [
        ShipDTO::from([
            'type' => 0,
            'size' => 5,
        ]),
        ShipDTO::from([
            'type' => 1,
            'size' => 4,
        ]),
        ShipDTO::from([
            'type' => 1,
            'size' => 4,
        ]),
    ],
];
