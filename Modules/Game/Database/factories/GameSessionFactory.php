<?php

namespace Modules\Game\Database\factories;

use Faker\Generator;
use Illuminate\Database\Eloquent\Factories\Factory;
use Modules\Game\Entities\GameSession;
use Modules\Game\Http\Services\GridService;

class GameSessionFactory extends Factory
{
    protected $model = GameSession::class;

    public function __construct(Generator $faker, private GridService $gridService)
    {
        parent::__construct($faker);
    }

    public function definition(): array
    {
        return [
            'uuid' => $this->faker->uuid,
            'player_grid' => $this->gridService->generateShipsGrid(),
            'computer_grid' => $this->gridService->generateShipsGrid(),
            'ended_at' => null,
        ];
    }
}
