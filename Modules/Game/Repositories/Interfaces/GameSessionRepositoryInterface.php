<?php

namespace Modules\Game\Repositories\Interfaces;

use Modules\Game\DTO\GameSessionDTO;
use Modules\Game\DTO\GridDTO;

interface GameSessionRepositoryInterface
{
    public function createGame(): string;

    public function loadGame(string $uuid): ?GameSessionDTO;

    public function saveHits(string $uuid, GridDTO $playerGrid, GridDTO $computerGrid): void;
}
