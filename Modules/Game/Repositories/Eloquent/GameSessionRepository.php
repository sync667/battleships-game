<?php

namespace Modules\Game\Repositories\Eloquent;

use Carbon\Carbon;
use Modules\Game\DTO\GameSessionDTO;
use Modules\Game\DTO\GridDTO;
use Modules\Game\Entities\GameSession;
use Modules\Game\Http\Services\GridService;
use Modules\Game\Repositories\Interfaces\GameSessionRepositoryInterface;

class GameSessionRepository implements GameSessionRepositoryInterface
{
    public function __construct(
        private GridService $gridService,
    ) {
    }

    public function createGame(): string
    {
        $gameSession = GameSession::create([
            'player_grid' => $this->gridService->generateShipsGrid(),
            'computer_grid' => $this->gridService->generateShipsGrid(),
        ]);

        return GameSession::where('id', $gameSession->id)->value('uuid');
    }

    public function loadGame(string $uuid): ?GameSessionDTO
    {
        $gameSession = GameSession::where('uuid', $uuid)->firstOrFail();

        return $gameSession ? GameSessionDTO::from([
            'uuid' => $gameSession->uuid,
            'player_grid' => $gameSession->player_grid,
            'computer_grid' => $gameSession->computer_grid,
            'ended_at' => $gameSession->ended_at,
        ]) : null;
    }

    public function saveHits(string $uuid, GridDTO $playerGrid, GridDTO $computerGrid): void
    {
        $gameSession = GameSession::where('uuid', $uuid)->firstOrFail();

        if ($gameSession === null) {
            return;
        }

        $gameSession->computer_grid = $computerGrid;
        $gameSession->player_grid = $playerGrid;
        $gameSession->save();
    }

    public function endGame(string $uuid): ?Carbon
    {
        $gameSession = GameSession::where('uuid', $uuid)->firstOrFail();

        if ($gameSession === null) {
            return null;
        }

        $gameSession->ended_at = Carbon::now('UTC');
        $gameSession->save();

        return $gameSession->ended_at;
    }
}
