<?php

namespace Modules\Game\DTO;

use Spatie\LaravelData\Attributes\DataCollectionOf;
use Spatie\LaravelData\Data;
use Spatie\LaravelData\DataCollection;

class GridDTO extends Data
{
    public int $size;

    #[DataCollectionOf(ShipDTO::class)]
    public ?DataCollection $ships;

    #[DataCollectionOf(HitDTO::class)]
    public ?DataCollection $hits;

    public function computerShoot(): ?DataCollection
    {
        //AI should be more complex for example if hit than shoot nearby
        $x = rand(0, $this->size - 1);
        $y = rand(0, $this->size - 1);

        if ($this->isAlreadyHit($x, $y)) {
            return $this->computerShoot();
        }

        return $this->shot($x, $y);
    }

    public function shot(int $x, int $y): ?DataCollection
    {
        if ($this->isAlreadyHit($x, $y)) {
            return null;
        }

        $this->hits ??= HitDTO::collection([]);
        $this->hitShip($x, $y);

        return $this->hits;
    }

    public function isAllShipsSunk(): bool
    {
        if (!$this->ships) {
            return true;
        }

        return $this->ships->filter(function ($ship) {
            return $ship->isSunk === false;
        })->count() === 0;
    }

    private function hitShip(int $x, int $y): void
    {
        foreach ($this->ships as $key => $ship) {
            $coords = $ship->coordinates->first(function (CoordsDTO $coords) use ($x, $y) {
                return $coords->x === $x && $coords->y === $y;
            });

            if($coords) {
                $this->hits[] = HitDTO::from([
                    'x' => $x,
                    'y' => $y,
                    'isMiss' => false,
                ]);

                if (!$ship->isSunk && $this->isShipSunk($ship)) {
                    $this->ships[$key]->isSunk = true;
                }

                return;
            }
        }

        $this->hits[] = HitDTO::from([
            'x' => $x,
            'y' => $y,
            'isMiss' => true,
        ]);
    }

    private function isAlreadyHit(int $x, int $y): bool
    {
        if (!$this->hits) {
            return false;
        }

        return $this->hits->first(
            fn (HitDTO $hit) => $hit->x === $x && $hit->y === $y
        ) !== null;
    }

    private function isShipSunk(ShipDTO $ship): bool
    {
        foreach ($ship->coordinates as $coords) {
            if (!$this->isAlreadyHit($coords->x, $coords->y)) {
                return false;
            }
        }

        return true;
    }
}
