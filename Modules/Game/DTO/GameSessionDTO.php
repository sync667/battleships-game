<?php

namespace Modules\Game\DTO;

use Spatie\LaravelData\Data;

class GameSessionDTO extends Data
{
    public string $uuid;
    public GridDTO $player_grid;
    public GridDTO $computer_grid;
    public ?string $ended_at;

    public function isGameEnded(): bool
    {
        return $this->ended_at !== null;
    }
}
