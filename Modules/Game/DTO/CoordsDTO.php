<?php

namespace Modules\Game\DTO;

use Spatie\LaravelData\Data;

class CoordsDTO extends Data
{
    public int $x;
    public int $y;
}
