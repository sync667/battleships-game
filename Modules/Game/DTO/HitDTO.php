<?php

namespace Modules\Game\DTO;

use Spatie\LaravelData\Data;

class HitDTO extends Data
{
    public int $x;
    public int $y;
    public bool $isMiss;
}
