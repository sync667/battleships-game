<?php

namespace Modules\Game\DTO;

use Spatie\LaravelData\Attributes\DataCollectionOf;
use Spatie\LaravelData\Data;
use Spatie\LaravelData\DataCollection;

class ShipDTO extends Data
{
    public int $type; //0 - Battleship, 1 - Destroyer #TODO move to enum
    public int $size;
    public bool $isSunk = false;

    #[DataCollectionOf(CoordsDTO::class)]
    public DataCollection $coordinates;
}
