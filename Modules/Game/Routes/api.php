<?php

use Modules\Game\Http\Controllers\GameActionsController;
use Modules\Game\Http\Controllers\GameController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'session', 'as' => 'game.session.'], function () {
    Route::get('create', [GameController::class, 'createSession'])->name('create');
    Route::post('load', [GameController::class, 'loadGame'])->name('load');
});

Route::group(['prefix' => 'actions', 'as' => 'game.actions.'], function () {
    Route::post('shoot', [GameActionsController::class, 'shoot'])->name('shoot');
});
