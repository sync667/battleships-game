## About Battleships

For simplifying back end front are in monorepo, but when using WSL front could need be copied to host.

App is divided intro PHP API and Angular frontend.
Of curse main focus at this point was on backend part.
In frontend, we have many visual aspect to be implemented, and it could be refactored into more seperated classes and components.

Normally to develop such a game as for company product this should be
at least couple seperated task to done it nice and clean. If there
was some talk in task description to do it more like one-side shooter then I could not say we are doing game, but just part of it, so I decided to do it more like full game as in video. Typically, in such not clear case I would ask business for more details or clearfy it with them.

Tests on frontend side where not completed due to lack of time (unusual medical problems), but I would do it with Jest and Cypress.

## How to run

API runs as docker containers.
1. Prepare .env file from .env.example and .env in ./docker folder.
2. Run after setting permissions if needed ./run up command.
3. Install needed dependencies with ./run composer install.
4. Restart docker containers with ./run restart command.
5. Run migrations with ./run artisan migrate command.
6. You should be able to access API on api.localhost
7. Run frontend with npm install && npm run start command from project folder ./frontend.
