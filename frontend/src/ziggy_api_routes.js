const Ziggy = {"url":"http:\/\/localhost","port":null,"defaults":{},"routes":{"scribe":{"uri":"docs","methods":["GET","HEAD"]},"scribe.postman":{"uri":"docs.postman","methods":["GET","HEAD"]},"scribe.openapi":{"uri":"docs.openapi","methods":["GET","HEAD"]},"sanctum.csrf-cookie":{"uri":"sanctum\/csrf-cookie","methods":["GET","HEAD"]},"ignition.healthCheck":{"uri":"_ignition\/health-check","methods":["GET","HEAD"]},"ignition.executeSolution":{"uri":"_ignition\/execute-solution","methods":["POST"]},"ignition.updateConfig":{"uri":"_ignition\/update-config","methods":["POST"]},"game.session.create":{"uri":"api\/v1\/game\/session\/create","methods":["GET","HEAD"]},"game.session.load":{"uri":"api\/v1\/game\/session\/load","methods":["POST"]},"game.actions.shoot":{"uri":"api\/v1\/game\/actions\/shoot","methods":["POST"]}}};

if (typeof window !== 'undefined' && typeof window.Ziggy !== 'undefined') {
    Object.assign(Ziggy.routes, window.Ziggy.routes);
}

export { Ziggy };
