export const environment = {
  production: true,
  api: {
    protocol: 'http',
    url: 'api.localhost',
  }
};
