import {HitModel} from "./hit.model";
import {ShipModel} from "./ship.model";

export class FieldModel {
  constructor(
    public id: number | null,
    public x: number,
    public y: number,
    public hit: HitModel | null = null,
    public ship: ShipModel | null = null
  ) {
  }
}
