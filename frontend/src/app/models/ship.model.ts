import {FieldModel} from "./field.model";

export class ShipModel {
  constructor(
    public type: number,
    public size: number,
    public isSunk: boolean,
    public coordinates: FieldModel[]
  ) {
  }
}
