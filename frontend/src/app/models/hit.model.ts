export class HitModel {
  constructor(
    public x: number,
    public y: number,
    public isMiss: boolean,
  ) {
  }
}
