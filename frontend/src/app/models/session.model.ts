import {GridModel} from "./grid.model";

export class SessionModel {
  constructor(
    public uuid: string,
    public player_grid: GridModel,
    public computer_grid: GridModel,
    public ended_at: string | null
  ) {
    this.player_grid = new GridModel(
      player_grid.hits,
      player_grid.ships,
      player_grid.size
    );

    this.computer_grid = new GridModel(
      computer_grid.hits,
      computer_grid.ships,
      computer_grid.size
    );
  }
}
