import {HitModel} from "./hit.model";
import {ShipModel} from "./ship.model";
import {FieldModel} from "./field.model";

export class GridModel {
  constructor(
    public hits: HitModel[],
    public ships: ShipModel[],
    public size: number,
  ) {
    let hitsArray: HitModel[] = [];

    hits?.forEach((hit: HitModel) => {
      hitsArray.push(new HitModel(
        hit.x,
        hit.y,
        hit.isMiss
      ));
    });

    this.hits = hitsArray;

    let shipsArray: ShipModel[] = [];

    ships?.forEach((ship: ShipModel) => {
      let coordinatesArray: FieldModel[] = [];

      ship.coordinates.forEach((field: FieldModel) => {
        coordinatesArray.push(new FieldModel(
          null,
          field.x,
          field.y,
        ));
      });

      shipsArray.push(new ShipModel(
        ship.type,
        ship.size,
        ship.isSunk,
        coordinatesArray
      ));
    });

    this.ships = shipsArray;
  }
}
