import {Injectable} from '@angular/core';
import route from 'ziggy-js';
// @ts-ignore
import {Ziggy} from '../../ziggy_api_routes';
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root',
})
export class ZiggyProviderService {
  public Ziggy: any = Ziggy;

  constructor() {
    Ziggy.url = environment.api.protocol + '://' + environment.api.url;
  }

  public route(name: string, params?: any): string {
    return route(name, params, true, this.Ziggy);
  }
}

