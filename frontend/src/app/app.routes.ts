import {Routes} from '@angular/router';

export const routes: Routes = [
  {
    path: 'game/:uuid',
    loadComponent: () => import('./game/game.page').then((m) => m.GamePage),
  },
  {
    path: 'intro',
    loadComponent: () => import('./intro/intro.page').then((m) => m.IntroPage),
  },
  {
    path: '**',
    redirectTo: 'intro',
    pathMatch: 'full',
  },
];
