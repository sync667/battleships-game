import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {ZiggyProviderService} from "../shared/ziggy-provider.service";
import {Observable} from "rxjs";
import {SessionModel} from "../models/session.model";

@Injectable({providedIn: 'root'})
export class GameService {
  constructor(
    private http: HttpClient,
    private ziggy: ZiggyProviderService,
  ) {
  }

  loadSession(sessionUuid: string): Observable<SessionModel> {
    return this.http.post<SessionModel>(
      this.ziggy.route('game.session.load'),
      {uuid: sessionUuid},
    );
  }

  shoot(sessionUuid: string, x: number, y: number): Observable<SessionModel> {
    return this.http.post<SessionModel>(
      this.ziggy.route('game.actions.shoot'),
      {uuid: sessionUuid, x: x, y: y},
    );
  }
}
