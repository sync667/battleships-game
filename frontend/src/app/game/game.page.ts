import {Component, OnInit} from '@angular/core';
import {IonicModule} from '@ionic/angular';
import {NgClass, NgForOf} from "@angular/common";
import {FieldModel} from "../models/field.model";
import {ActivatedRoute, ParamMap, Router} from "@angular/router";
import {GameService} from "./game.service";
import {HttpClientModule} from "@angular/common/http";
import {SessionModel} from "../models/session.model";
import {ShipModel} from "../models/ship.model";
import {HitModel} from "../models/hit.model";

@Component({
  selector: 'app-game',
  templateUrl: 'game.page.html',
  styleUrls: ['game.page.scss'],
  standalone: true,
  imports: [IonicModule, NgForOf, NgClass, HttpClientModule],
})
export class GamePage implements OnInit {
  public yourGrid: FieldModel[] = [];
  public enemyGrid: FieldModel[] = [];
  public selectedField: null | FieldModel = null
  public uuid: string = '';
  public gameSession: SessionModel | undefined;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private gameService: GameService) {
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.uuid = params.get('uuid') ?? '';
    });
  }

  public ngOnInit(): void {
    this.gameService.loadSession(this.uuid).subscribe(
      (response) => {
        this.gameSession = new SessionModel(
          response.uuid,
          response.player_grid,
          response.computer_grid,
          response.ended_at,
        )

        this.refreshGrids();
      },
      (error) => {
        this.router.navigate(['/game']);
        return;
      });
  }

  public selectField(field: FieldModel): void {
    if (this.gameSession?.ended_at !== null) {
      return;
    }

    this.selectedField = this.selectedField?.id === field.id ? null : field;
  }

  public shoot(): void {
    if (!this.selectedField) {
      return;
    }

    this.gameService.shoot(this.uuid, this.selectedField.x, this.selectedField.y).subscribe((response) => {
      this.gameSession = new SessionModel(
        response.uuid,
        response.player_grid,
        response.computer_grid,
        response.ended_at,
      )

      this.selectedField = null;
      this.refreshGrids();
    });
  }

  public newGame(): void {
    this.router.navigate(['/intro']);
  }

  private refreshGrids(): void {
    this.yourGrid = [];
    this.enemyGrid = [];

    for (let x = 0; x <= 9; x++) {
      for (let y = 0; y <= 9; y++) {

        const foundShip: ShipModel | undefined = this.gameSession?.player_grid.ships.find((ship: ShipModel) => {
          return ship.coordinates.find((field: FieldModel) => field.x === x && field.y === y);
        });

        const foundHit: HitModel | undefined = this.gameSession?.player_grid.hits.find((hit: HitModel) => {
          return hit.x === x && hit.y === y;
        });

        if (foundShip || foundHit) {
          this.yourGrid.push(new FieldModel(this.yourGrid.length + 1, x, y, foundHit, foundShip));
        } else {
          this.yourGrid.push(new FieldModel(this.yourGrid.length + 1, x, y));
        }

        const foundEnemyShip: ShipModel | undefined = this.gameSession?.computer_grid.ships.find((ship: ShipModel) => {
          return ship.coordinates.find((field: FieldModel) => field.x === x && field.y === y);
        });

        const foundEnemyHit: HitModel | undefined = this.gameSession?.computer_grid.hits.find((hit: HitModel) => {
          return hit.x === x && hit.y === y;
        });

        if (foundEnemyShip || foundEnemyHit) {
          this.enemyGrid.push(new FieldModel(this.enemyGrid.length + 1, x, y, foundEnemyHit, foundEnemyShip));
        } else {
          this.enemyGrid.push(new FieldModel(this.enemyGrid.length + 1, x, y));
        }
      }
    }
  }
}
