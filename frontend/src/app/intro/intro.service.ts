import {Injectable} from "@angular/core";
import {HttpClient, HttpResponse} from "@angular/common/http";
import {ZiggyProviderService} from "../shared/ziggy-provider.service";
import {Observable} from "rxjs";

export type EntityResponseType = HttpResponse<any>;

@Injectable({providedIn: 'root'})
export class IntroService {
  constructor(
    private http: HttpClient,
    private ziggy: ZiggyProviderService,
  ) {
  }

  createSession(): Observable<EntityResponseType> {
    return this.http.get<string>(
      this.ziggy.route('game.session.create'),
      {observe: 'response'},
    );
  }
}
