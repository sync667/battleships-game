import {Component} from '@angular/core';
import {IonicModule} from '@ionic/angular';
import {NgClass, NgForOf} from "@angular/common";
import {Router} from "@angular/router";
import {IntroService} from "./intro.service";
import {HttpClientModule} from "@angular/common/http";

@Component({
  selector: 'app-intro',
  templateUrl: 'intro.page.html',
  styleUrls: ['intro.page.scss'],
  standalone: true,
  imports: [IonicModule, NgForOf, NgClass, HttpClientModule],
})
export class IntroPage {
  constructor(private router: Router,
              private introService: IntroService) {
  }

  public startGame(): void {
    this.introService.createSession().subscribe((response) => {
      this.router.navigate(['/game', response.body.uuid]);
    });

    return;
  }
}
