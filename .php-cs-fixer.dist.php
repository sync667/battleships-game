<?php

$config = new PhpCsFixer\Config();
return $config
    ->setRiskyAllowed(true)
    ->setUsingCache(false)
    ->setRules([
                   '@PSR12' => true,
                   'array_indentation' => true,
                   'blank_line_before_statement' => true,
                   'cast_spaces' => true,
                   'class_attributes_separation' => ['elements' =>
                       ['const' => 'only_if_meta',
                           'method' => 'one',
                           'property' => 'only_if_meta',
                           'trait_import' => 'none'
                       ]
                   ],
                   'declare_strict_types' => false,
                   'method_chaining_indentation' => true,
                   'no_unused_imports' => true,
                   'ordered_imports' => true,
                   'trailing_comma_in_multiline' => true,
               ])
    ->setFinder(
        PhpCsFixer\Finder::create()
            ->in(['app', 'config', 'database', 'Modules', 'tests', 'public'])
    );
