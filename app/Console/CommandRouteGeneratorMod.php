<?php

namespace App\Console;

use Illuminate\Filesystem\Filesystem;
use Tightenco\Ziggy\CommandRouteGenerator;
use Tightenco\Ziggy\Ziggy;

class CommandRouteGeneratorMod extends CommandRouteGenerator
{
    protected $signature = 'mziggy:generate {path=./resources/assets/js/ziggy.js} {--group=}';

    public function __construct(Filesystem $files)
    {
        $this->signature .= ' {--url=' . config('app.url') . '}';
        parent::__construct($files);

        $this->files = $files;
    }

    public function handle(): void
    {
        $path = $this->argument('path');
        $group = $this->option('group');

        if ($group) {//ignores path
            $path = './ziggy_' . $group . '_routes.js';
        }

        $generatedRoutes = $this->generate($group);

        $this->makeDirectory($path);

        $this->files->put(base_path($path), $generatedRoutes);
    }

    protected function makeDirectory($path)
    {
        if (!$this->files->isDirectory(dirname(base_path($path)))) {
            $this->files->makeDirectory(dirname(base_path($path)), 0755, true, true);
        }

        return $path;
    }

    private function generate($group = false): string
    {
        $payload = (new Ziggy($group, $this->option('url') ? url($this->option('url')) : null))->toJson();

        return <<<JAVASCRIPT
const Ziggy = {$payload};

if (typeof window !== 'undefined' && typeof window.Ziggy !== 'undefined') {
    Object.assign(Ziggy.routes, window.Ziggy.routes);
}

export { Ziggy };

JAVASCRIPT;
    }
}
