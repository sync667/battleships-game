<?php

namespace App\Providers;

use App\Console\CommandRouteGeneratorMod;
use Illuminate\View\Compilers\BladeCompiler;
use Tightenco\Ziggy\ZiggyServiceProvider;

class ZiggyServiceProviderExtended extends ZiggyServiceProvider
{
    public function boot()
    {
        if ($this->app->resolved('blade.compiler')) {
            $this->registerDirective($this->app['blade.compiler']);
        } else {
            $this->app->afterResolving('blade.compiler', function (BladeCompiler $bladeCompiler) {
                $this->registerDirective($bladeCompiler);
            });
        }

        if ($this->app->runningInConsole()) {
            $this->commands(CommandRouteGeneratorMod::class);
        }
    }
}
