version: '3.9'

networks:
  app:
    driver: bridge
volumes:
  code:
    driver: local
  pgsql:
    driver: local
  pgsql-test:
    driver: local
  redis:
    driver: local
  redisinsight:
    driver: local

services:
  redisinsight:
    image: redislabs/redisinsight:latest
    ports:
      - '${REDISINSIGHT_PORT:-8001}:8001'
    volumes:
      - redisinsight:/db
    networks:
      - app
  dev-app:
    image: registry.gitlab.com/medcases1/medcases-php/dev-app:latest
    build:
      context: local/app
      dockerfile: Dockerfile
      target: php-dev-app
    environment:
      GITLAB_TOKEN: '${GITLAB_TOKEN}'
      GITHUB_TOKEN: '${GITHUB_TOKEN}'
      PHP_IDE_CONFIG: 'serverName=${PHP_IDE_SERVER:-localhost}'
    ports:
      - '${APP_PORT:-80}:80'
      - '${APP_SSL_PORT:-443}:443'
    volumes:
      - ../:${CODE_PATH_CONTAINER}
      - ./local/app/php-ini/xdebug.ini:/usr/local/etc/php/conf.d/xdebug.ini
    networks:
      - app
    depends_on:
      - pgsql
      - redis
  dev-app-octane:
    image: registry.gitlab.com/medcases1/medcases-php/dev-app-octane:latest
    build:
      context: local/app-octane
      dockerfile: Dockerfile
      target: php-dev-app
    environment:
      GITLAB_TOKEN: '${GITLAB_TOKEN}'
      GITHUB_TOKEN: '${GITHUB_TOKEN}'
      PHP_IDE_CONFIG: 'serverName=${PHP_IDE_SERVER:-localhost}'
    ports:
      - '${APP_PORT:-80}:80'
    volumes:
      - ../:${CODE_PATH_CONTAINER}
      - ./local/app-octane/php-ini/xdebug.ini:/usr/local/etc/php/conf.d/xdebug.ini
    networks:
      - app
    depends_on:
      - pgsql
      - redis
  dev-workers:
    image: registry.gitlab.com/medcases1/medcases-php/dev-workers:latest
    build:
      context: local/workers
      dockerfile: Dockerfile
      target: php-dev-workers
    environment:
      GITLAB_TOKEN: '${GITLAB_TOKEN}'
      GITHUB_TOKEN: '${GITHUB_TOKEN}'
    volumes:
      - ../:${CODE_PATH_CONTAINER}
    networks:
      - app
    depends_on:
      - pgsql
      - redis
  dev-socket:
    image: registry.gitlab.com/medcases1/medcases-php/dev-socket:latest
    build:
      context: local/socket
      dockerfile: Dockerfile
      target: socket-dev
    ports:
      - '${SOCKET_PORT:-6001}:6001'
      - '${SOCKET_USAGE_PORT:-9601}:9601'
    volumes:
      - ../:${CODE_PATH_CONTAINER}
    networks:
      - app
    depends_on:
      - pgsql
      - redis
  pgsql:
    image: 'postgres:13'
    ports:
      - '${FORWARD_DB_PORT:-5433}:5432'
    environment:
      POSTGRES_DB: '${DB_DATABASE}'
      POSTGRES_USER: '${DB_USERNAME}'
      POSTGRES_PASSWORD: '${DB_PASSWORD}'
    volumes:
      - 'pgsql:/var/lib/postgresql/data'
    networks:
      - app
  pgsql-test:
    image: 'postgres:13'
    ports:
      - '${FORWARD_TEST_DB_PORT:-5432}:5432'
    environment:
      POSTGRES_DB: '${DB_TEST_DATABASE}'
      POSTGRES_USER: '${DB_USERNAME}'
      POSTGRES_PASSWORD: '${DB_PASSWORD}'
    volumes:
      - 'pgsql-test:/var/lib/postgresql/data'
    networks:
      - app
  redis:
    image: registry.gitlab.com/medcases1/medcases-php/redis:latest
    build:
      context: redis
    environment:
      REDIS_PASSWORD: '${REDIS_PASS}'
    ports:
      - '${REDIS_PORT:-6379}:6379'
    volumes:
      - 'redis:/data'
      - ./redis/keydb.conf:/etc/keydb/keydb.conf
    networks:
      - app
  mailhog:
    image: 'mailhog/mailhog:latest'
    ports:
      - '${MAILHOG_PORT:-8025}:8025'
      - '${MAILHOG_SMTP_PORT:-1025}:1025'
    networks:
      - app
  base-dev-app:
    build:
      context: local/app
      dockerfile: Dockerfile
      target: base-dev-app
    networks:
      - app
  base-dev-workers:
    build:
      context: local/workers
      dockerfile: Dockerfile
      target: base-dev-workers
    networks:
      - app
  docker-with-compose:
    image: registry.gitlab.com/medcases1/it/medcases-php/docker-with-compose:latest
    build:
      context: docker-with-compose
    networks:
      - app
