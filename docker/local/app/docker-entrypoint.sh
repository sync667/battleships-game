#!/usr/bin/env bash

if [ ! -z "$GITLAB_TOKEN" ]; then
  composer config --global gitlab-token.gitlab.com $GITLAB_TOKEN
fi

if [ ! -z "$GITHUB_TOKEN" ]; then
  composer config --global github-oauth.github.com $GITHUB_TOKEN
fi

echo -e "$(/sbin/ip route | awk '/default/ { print $3 }')\tdocker.host.internal" | sudo tee -a /etc/hosts >/dev/null
echo -e "$(/sbin/ip route | awk '/default/ { print $3 }')\thost.docker.internal" | sudo tee -a /etc/hosts >/dev/null
