user  www-data;
worker_processes  auto;
daemon off;

error_log  /var/log/nginx/error.log;
pid        /var/run/nginx.pid;

events {
    worker_connections  1024;  ## Default: 1024
}

http {
  include       /etc/nginx/mime.types;
  default_type  application/octet-stream;

  log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                        '$status $body_bytes_sent "$http_referer" '
                        '"$http_user_agent" "$http_x_forwarded_for"';

  access_log  /var/www/storage/logs/access.log  main;
  error_log  /var/www/storage/logs/error.log debug;

  sendfile on;
  keepalive_timeout  65;

  server {
    listen 80 default_server;
    listen 443 ssl default_server;

    ssl_certificate /etc/nginx/ssl/nginx-selfsigned.crt;
    ssl_certificate_key /etc/nginx/ssl/nginx-selfsigned.key;

    server_tokens off;
    index index.php;

    root /var/www/public;

    charset utf-8;
    client_max_body_size 100M;

    location / {
       try_files $uri /index.php$is_args$args;
    }

    location ~ ^/index\.php(/|$) {
       fastcgi_pass localhost:9000;
       fastcgi_split_path_info ^(.+\.php)(/.*)$;
       include fastcgi_params;
       fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;
       fastcgi_param DOCUMENT_ROOT $realpath_root;
       internal;
    }

    # return 404 for all other php files not matching the front controller
    # this prevents access to other php files you don't want to be accessible.
    location ~ \.php$ {
       return 404;
    }

    location = /favicon.ico { access_log off; log_not_found off; }
    location = /robots.txt  { access_log off; log_not_found off; }
  }
}
